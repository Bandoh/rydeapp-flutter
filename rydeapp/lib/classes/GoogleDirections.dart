import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';



class GoogleDirections{
String apikey;
String directionsurl;
String initialposition;
String finalposition;
Map<String,dynamic> cord;

  GoogleDirections(String apikey){
    this.apikey = apikey;
  }



  Future<List<dynamic>> polyCord(String initialposition,String finalposition) async{
    var client = new http.Client();
        directionsurl = 'http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=$initialposition&wp.1=$finalposition&routeAttributes=routePath&key=AlttOIj3OoyyPhflwbhGOlz3hkuW3QLP2pN5k_-pDTErmXvhSYDxUSsW8NCGd50f';
    var response = await client.get(directionsurl);
    cord = json.decode(response.body);
  //  print(cord['resourceSets'][0]['resources'][0]['routePath']['line']['coordinates']);
    return cord['resourceSets'][0]['resources'][0]['routePath']['line']['coordinates'];
  }

}
