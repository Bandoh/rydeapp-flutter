import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:map_view/polyline.dart';
import 'package:rydeapp/classes/GoogleDirections.dart';
import 'package:rydeapp/main.dart';

class MapPageClass {
    var car = AssetImage('assets/sedan.ico');
  GoogleDirections n = new GoogleDirections(apikey);
  List<dynamic> curves;
  List<List<dynamic>> curvesList;
  List<dynamic> keep = [];

  String _start;
  String _end;
  double _zoom;
  BuildContext _context;
  // double _elong;
  // double _elat;
  int _noMarkers;
  List<Map<String, dynamic>> _markerinfo;
  List<Map<String, double>> _routes;
  List<double> _st;

  MapPageClass.trips(
      {String start,
      String end,
      double elatitude,
      double elongitude,
      BuildContext context,
      double zoom}) {
    this._start = start;
    this._end = end;
    this._zoom = zoom;
    this._context=context;
  }

  MapPageClass.buslocation(
      {String start,
      String end,
      List<Map<String, double>> routes,
      double zoom}) {
    this._routes = routes;
    this._start = start;
    this._end = end;
    this._zoom = zoom;
  }

  MapPageClass.formarkers(
      {int number,
      List<Map<String, dynamic>> points,
      String start,
      String end,
      double zoom}) {
    this._noMarkers = number;
    this._markerinfo = points;
    this._start = start;
    this._end = end;
    this._zoom = zoom;
  }

  MapView mapView = new MapView();
  showmap() async {
    _st = splitting(this._start);

    await getroutes();
    mapView.show(
        new MapOptions(
            showMyLocationButton: true,
            initialCameraPosition:
                new CameraPosition(new Location(_st[0], _st[1]), this._zoom),
            mapViewType: MapViewType.normal,
            showUserLocation: true,
            title: "Available Rydes"),
        toolbarActions: [new ToolbarAction('Close', 1)]);
    mapView.onMapReady.listen((_) {
      try {
        mapView.setMarkers(_trymark());
      } catch (e) {
        
      }
      mapView.setPolylines(_trypoly(this._context));
    });
    mapView.onToolbarAction.listen((id) {
      if (id == 1) {
        mapView.dismiss();
      }
    });
  }

  // getroute() async {
  //   curves = await n.polyCord(
  //       '${this._lat.toString()},${this._long.toString()}',
  //       '5.6712962,-0.2153185');
  // }

  getroutes() async {
    try {
      print(this._markerinfo[2]['latitude'] +
          '' +
          this._markerinfo[2]['longitude']);
      for (int i = 0; i < this._routes.length; i++) {
        //curves = await n.polyCord(
        // '${this._lat.toString()},${this._long.toString()}',
        // '${this._routes[i]['latitude'].toString()},${this._routes[i]['longitude'].toString()}');
        //  keep.add(curves);
      }
      // curves = await n.polyCord(
      //     '${this._lat.toString()},${this._long.toString()}',
      //     '5.6712962,-0.2153185');
    } catch (e) {
      curves = await n.polyCord(
          '${this._start.toString()}', '${this._end.toString()}');
      // print(curves);
    }
  }

  List<Polyline> _trypoly(BuildContext context) {
    List<Polyline> polyline = [];
    List<Location> polyloc = [];
    try {
      for (int i = 0; i < curves.length; i++) {
        polyloc.add(new Location(curves[i][0], curves[i][1],));
      }
      polyline.add(new Polyline('1', polyloc, width: Theme.of(context).platform == TargetPlatform.iOS? 7.0:10.0, color: Colors.blueAccent));
    } catch (e) {}
    return polyline;
  }

  List<Marker> _trymark() {
    List<Marker> markers = [];
    try {
      
      for (int i = 0; i < this._noMarkers; i++) {
        print('${this._markerinfo[i]['latitude']}\n\n');
        if (this._markerinfo[i]['areatype'].toString().contains('esidenti')) {
          markers.add(new Marker('$i', 'Station',
              this._markerinfo[i]['latitude'], this._markerinfo[i]['longitude'],
              color: Colors.blue));
        } else {
          markers.add(new Marker('$i', 'Station',
              this._markerinfo[i]['latitude'], this._markerinfo[i]['longitude'],
              color: Colors.red,));
        }
      }
      print(markers);
      return markers;
    } catch (e) {
      print('caught');
      markers.add(new Marker('1', 'Source', curves[0][0], curves[0][1], color: Colors.orange,)); 
      markers.add(new Marker('1', 'Source', curves[curves.length-1][0], curves[curves.length-1][1], color: Colors.blue)); 
      return markers;
    }
  }

  List<double> splitting(String locc) {
    List<double> l = [];
    List<String> loc = locc.split(",");
    for (int i = 0; i <= loc.length - 1; i++) {
      l.add(double.parse(loc[i]));
    }
    return l;
  }
}
