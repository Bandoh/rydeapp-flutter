import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/Interfaces/login.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/classes/mappageclass.dart';
import 'package:rydeapp/main.dart';
import 'package:http/http.dart' as http;

List<String> loca = [];
String dest;
String start;
MapView mapView = new MapView();

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  TextEditingController seats = new TextEditingController();
  List<Map<String, dynamic>> loc = [];

  String price;

  void _onchangeprice(String value) {
    setState(() {
      price = value;
    });
  }

  String content =
      'We provide a convinient and reliable transport and solution-logistics modules for you';
  var backg = new AssetImage('assets/danfoman.jpg');
  var bus = new AssetImage('assets/bus.png');
  Map<String, dynamic> getroutes;

  double leftpadding = 10.0;

  bool c;

  int lengthget = 0;
  bool _isLoading = true;

  getroute() async {
    var client = new http.Client();
    var response = await client.get('https://ryde-rest.herokuapp.com/alltrips',
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          'x-access-token': token
        });
    getroutes = await json.decode(response.body);
    setState(() {
      try {
        _isLoading = false;
        lengthget = getroutes['trips'].length;
      } catch (e) {
        lengthget = 0;
      }
    });
  }

  String from;
  String to;
  @override
  void initState() {
    price = prices.elementAt(0);
    getroute();
    from = places.elementAt(0);
    to = places.elementAt(0);
    super.initState();
  }

  showmapp() {
    mapView.show(MapOptions(
        showUserLocation: true,
        initialCameraPosition: new CameraPosition(
            new Location(userlocation['latitude'], userlocation['longitude']),
            15.0)));
  }
  // @override
  // void dispose() {
  //   _control.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // drawer: new Container(
      //     decoration: new BoxDecoration(color: Colors.white),
      //     child: new Column(
      //       crossAxisAlignment: CrossAxisAlignment.start,
      //       children: <Widget>[
      //         new Card(
      //           elevation: 3.0,
      //           child: new Container(
      //             decoration: new BoxDecoration(color: Colors.orange),
      //             //margin: EdgeInsets.all(30.0),
      //             padding: EdgeInsets.all(60.0),
      //             child: new CircleAvatar(
      //               backgroundColor: Colors.red,
      //               child: new Image(
      //                 image: bus,
      //                 height: 100.0,
      //               ),r
      //               radius: 110.0,
      //             ),
      //           ),
      //         ),
      //         new Card(
      //           margin: EdgeInsets.only(top: 10.0),
      //           elevation: 1.0,
      //           child: new Container(
      //             width: 340.0,
      //             padding: EdgeInsets.all(15.0),
      //             child: new Text(
      //               'Full name: ${user['FULL_NAME']}',
      //               style: new TextStyle(fontSize: 20.0),
      //             ),
      //           ),
      //         ),
      //         new Card(
      //           margin: EdgeInsets.only(top: 10.0),
      //           elevation: 1.0,
      //           child: new Container(
      //             width: 340.0,
      //             padding: EdgeInsets.all(15.0),
      //             child: new Text(
      //               'Email: ${user['EMAIL']}',
      //               style: new TextStyle(fontSize: 20.0),
      //             ),
      //           ),
      //         ),
      //                       new Card(
      //           margin: EdgeInsets.only(top: 10.0),
      //           elevation: 1.0,
      //           child: new Container(
      //             width: 340.0,
      //             padding: EdgeInsets.all(15.0),
      //             child: new Text(
      //               'Contact: ${user['PHONE_NUMBER']}',
      //               style: new TextStyle(fontSize: 20.0),
      //             ),
      //           ),
      //         ),
      //       ],
      //     )),
      body: new RefreshIndicator(
        onRefresh: () async {
          await getroute();

          print('getting New Routes!');
          return Future.delayed(new Duration(milliseconds: 1000));
        },
        child: new CustomScrollView(
          slivers: <Widget>[
            new SliverAppBar(
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                flexibleSpace: new FlexibleSpaceBar(
                  background: new Image(
                    image: backg,
                    fit: BoxFit.cover,
                  ),
                  title: new Text(
                    'Available Rydes!',
                    style: new TextStyle(color: Colors.white),
                  ),
                )),
            _isLoading
                ? new SliverFillRemaining(
                    child: new Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.red,
                    ),
                  ))
                : new SliverList(
                    delegate: new SliverChildBuilderDelegate((context, index) {
                      return new Card(
                        elevation: 0.5,
                        child: new GestureDetector(
                          //onHorizontalDragStart: startDrag,
                          //getloc = getroutes['ROUTES'];
                          child: ListTile(
                            enabled: true,
                            //leading: new Icon(Icons.directions_car, color: Colors.orangeAccent, semanticLabel: 'Trips',),
                            title: new Text(
                                getroutes['trips'][index]['ROUTE'] == null
                                    ? 0
                                    : getroutes['trips'][index]['ROUTE']),
                            onLongPress: () {
                              showroutedetail(index);
                            },
                            onTap: () {},
                          ),
                        ),
                      );
                    }, childCount: lengthget == null ? 0 : lengthget),
                  ),
          ],
        ),
      ),
      // floatingActionButton: new FloatingActionButton(
      //   onPressed: () {
      //     getroute();
      //   },
      //   child: new Icon(
      //     Icons.refresh,
      //     color: Colors.black,
      //   ),
      //   backgroundColor: Colors.orange,
      //   mini: true,
      // ),
    );
  }

  void showroutedetail(int ind) {
    var alertd = new AlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      //title: new Text('Route Information'),
      content: new Container( 
        height: MediaQuery.of(context).size.height / 2.5,
        width: MediaQuery.of(context).size.width,
        child: new ListView(
          // mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // new Container(
            //   margin: EdgeInsets.only(bottom: 20.0),
            //   height: 20.0,
            //   decoration: new BoxDecoration(
            //     color: Colors.orange,
            //     //image: new DecorationImage(image: backg, fit: BoxFit.cover),
            //   ),
            // ),
            new Container(
              margin: EdgeInsets.all(10.0),
              child: new Text(
                'Route: ${getroutes['trips'][ind]['ROUTE']}',
                style: new TextStyle(fontSize: 13.0),
              ),
            ),
            new Container(
              margin: EdgeInsets.all(10.0),
              child: new Text(
                'Morning Departure Time: ${getroutes['trips'][ind]['MORNING']}',
                style: new TextStyle(fontSize: 12.0),
              ),
            ),
            new Container(
              margin: EdgeInsets.all(10.0),
              child: new Text(
                'Evening Departure Time: ${getroutes['trips'][ind]['EVENING']}',
                style: new TextStyle(fontSize: 12.0),
              ),
            ),
            new Container(
              margin: EdgeInsets.all(10.0),
              child: new Text(
                'Seats Remaining: ${getroutes['trips'][ind]['SEATS']}',
                style: new TextStyle(fontSize: 12.0),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 10.0),
              child: new Text(
                'Zone: ${getroutes['trips'][ind]['zone']['NAME']}',
                style: new TextStyle(fontSize: 12.0),
              ),
            ),
            new Container(
                margin: EdgeInsets.only(left: 10.0, right: 10.0),
                child: new DropdownButton(
                  onChanged: (dynamic value) {
                    _onchangeprice(value);
                  },
                  value: price,
                  items: prices.map((String value) {
                    return new DropdownMenuItem(
                      value: value,
                      child: new Container(
                        //width: screenwidth * 0.5,
                        child: new Text(
                          value,
                          style: new TextStyle(fontSize: 12.0),
                        ),
                      ),
                    );
                  }).toList(),
                )),

            new Form(
              key: _formKey,
              child: new Container(
                margin: EdgeInsets.only(
                  left: leftpadding,
                  right: leftpadding * 3,
                  //bottom: leftpadding
                ),
                child: new TextFormField(
                  keyboardType: TextInputType.number,
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Please Inout Seat';
                    } else if (int.parse(val) >
                        int.parse('${getroutes['trips'][ind]['SEATS']}')) {
                      return '${getroutes['trips'][ind]['SEATS']} Remaining';
                    } else {
                      return null;
                    }
                  },
                  controller: seats,
                  decoration: new InputDecoration(
                      labelText: 'Seat: ',
                      labelStyle: new TextStyle(color: Colors.orange)),
                ),
              ),
            ),
            // new Container(
            //   child: new QrImage(
            //     data: 'Bandoh Developed This',
            //     size: 150.0,
            //   ),
            // ),
          ],
        ),
      ),
      actions: <Widget>[
        new Container(
          child: new FlatButton(
            onPressed: () {
              MapPageClass mapPageClass = new MapPageClass.trips(
                  start: getroutes['trips'][ind]['geoData']['STARTLATLONG'],
                  end: getroutes['trips'][ind]['geoData']['ENDLATLONG'],
                  zoom: 11.5,
                  context: context);
              mapPageClass.showmap();
            },
            child: new Text(
              'Preview in Map',
              style: new TextStyle(
                color: Colors.orange,
                //fontSize: 13.0,
              ),
            ),
          ),
        ),
        new Container(
          child: new FlatButton(
            onPressed: () {
              final form = _formKey.currentState;
              if (form.validate()) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Payscreen(
                            seatN: seats.text.trim(),
                            price: price,
                            description: 'Paying for a Trip',
                            tripid: '${getroutes['trips'][ind]['id']}',
                            paymentType: 'trip',
                            item: '${getroutes['trips'][ind]['ROUTE']}',
                          )),
                );
              }
            },
            child: new Text(
              'Book Ryde',
              style: new TextStyle(
                color: Colors.orange,
                // fontSize: 13.0,
              ),
            ),
          ),
        ),
      ],
    );

    showDialog(
      context: context,
      child: alertd,
    );
  }
}
