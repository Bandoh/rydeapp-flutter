import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/classes/qrimage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

bool _triploading = true;
bool _rentalloading = true;
bool _orgloading = true;

int _rentslength = 0;
int _orglength = 0;
int _tripslength = 0;

Map<String, dynamic> _bodytrips;
Map<String, dynamic> _bodyrents;
Map<String, dynamic> _bodyorg;

class _ThirdPageState extends State<ThirdPage> {
  //Trips Page layout
  getttrip() async {
    GetTrip gt = new GetTrip();
    await gt.gettrips();
  }

  Widget tripstab(List<dynamic> body, int count, ImageProvider im) {
    getttrip();
    try {
      if (count == 0) {
        return new RefreshIndicator(
          onRefresh: () async {
            GetTrip gt = new GetTrip();
            await gt.gettrips();
            setState(() {
              _triploading;
              _bodytrips;
            });
            return new Future.delayed(const Duration(milliseconds: 1000));
          },
          child: new Center(
            child: new ListView(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 2.8),
              children: <Widget>[new Center(child: Text('No data'))],
            ),
          ),
        );
      } else {
        return new Container(
            margin: EdgeInsets.all(10.0),
            child: new RefreshIndicator(
              onRefresh: () async {
                GetTrip gt = new GetTrip();
                await gt.gettrips();
                setState(() {
                  _triploading;
                  _bodytrips;
                });
                return new Future.delayed(const Duration(milliseconds: 1000));
              },
              child: new ListView.builder(
                itemCount: count == null ? 0 : count,
                itemBuilder: (BuildContext context, int index) {
                  return new GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('/tripinfo');
                    },
                    child: new Card(
                      // color: Colors.orange[100],
                      elevation: 9.0,
                      child: new Container(
                        child: new ListTile(
                          onTap: () {
                            AlertDialog a = new AlertDialog(
                              content: new Container(
                                height: MediaQuery.of(context).size.height / 4,
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                          'Route: ${body[index]['ROUTE']}', style: new TextStyle(
                                       
                                       fontSize: 12.0
                                    ),),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                          'Morning Departure Time: ${body[index]['MORNING']}', style: new TextStyle(
                                       
                                       fontSize: 12.0
                                    ),),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                          'Evening Departure Time: ${body[index]['EVENING']}', style: new TextStyle(
                                       
                                       fontSize: 12.0
                                    ),),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: new Text(
                                          'Seats Checkedout: ${body[index]['user_trip']['SEATS']}', style: new TextStyle(
                                       
                                       fontSize: 12.0
                                    ),),
                                    ),
                                  ],
                                ),
                              ),
                            );
                            showDialog(
                              context: context,
                              child: a,
                            );
                          },
                          onLongPress: () {
                            int days =
                                int.parse((body[index]['user_trip']['EXP']));
                            days = (days / (1000 * 60 * 60 * 24) % 7).toInt();
                            var qr = new AlertDialog(
                               contentPadding: EdgeInsets.all(0.0),
                                title: new Text(
                                    'Ticket:  ${body[index]['ROUTE']}', style: new TextStyle(
                                       
                                       fontSize: 12.0
                                    ),),
                                content: new Container(
                                    height: 350.0,
                                    //width: 200.0,
                                    child: new Container(
                                        child: new Column(
                                      children: <Widget>[
                                        new Container(
                                          child: new QrImage(
                                            version: 5,
                                            data:
                                                '${body[index]['ROUTE']}>${body[index]['id']}>${body[index]['user_trip']['EXP']}',
                                            size: 300.0,
                                           // padding: EdgeInsets.only(left: 8.0),
                                          ),
                                        ),
                                        new Container(
                                          child:
                                              new Text('Expires in $days days', style: new TextStyle(
                                                 fontSize:12.0
                                              ),),
                                        ),
                                      ],
                                    ))));
                            showDialog(
                              child: qr,
                              context: context,
                            );
                          },
                          leading: new Image(
                            image: bus,
                            height: 35.0,
                            color: Colors.orangeAccent,
                          ),
                          title: new Text(
                            '${body[index]['ROUTE']}',
                            style: new TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange),
                          ),
                          isThreeLine: true,
                          subtitle: new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                margin: EdgeInsets.all(2.0),
                                child: new Text(
                                  'Morning Time: ${body[index]['MORNING']}',
                                  style: new TextStyle(
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.all(2.0),
                                child: new Text(
                                  'Evening Time: ${body[index]['EVENING']}',
                                  style: new TextStyle(
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ));
      }
    } catch (e) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    }
  }

  Widget orgpage(List<dynamic> body, int count) {
    try {
      if (count == 0) {
        return new RefreshIndicator(
          onRefresh: () async {
            GetOrganised getOrganised = new GetOrganised();
            await getOrganised.getorganised();
            setState(() {
              _orglength;
            });
            return new Future.delayed(const Duration(milliseconds: 1000));
          },
          child: new Center(
            child: new ListView(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 2.8),
              children: <Widget>[new Center(child: Text('No data'))],
            ),
          ),
        );
      } else {
        return new GestureDetector(
          onTap: () {
            GetPassengers gp = new GetPassengers();
            gp.getjoined();
          },
          child: new Container(
            child: new RefreshIndicator(
              onRefresh: () async {
                GetOrganised getOrganised = new GetOrganised();
                await getOrganised.getorganised();
                setState(() {
                  _orglength;
                });
                return new Future.delayed(const Duration(milliseconds: 1000));
              },
              child: new ListView.builder(
                  itemCount: count,
                  itemBuilder: (BuildContext context, int index) {
                    return new Container(
                      height: MediaQuery.of(context).size.height / 3.5,
                      child: new Card(
                        margin: EdgeInsets.all(15.0),
                        elevation: 7.0,
                        child: new Container(
                            child: new Row(
                          children: <Widget>[
                            new Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: 50.0,
                              decoration: new BoxDecoration(
                                  color: Colors.orangeAccent[200]),
                            ),
                            new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  height: 10.0,
                                  // decoration: new BoxDecoration(color: Colors.teal[100]),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  margin: EdgeInsets.all(5.0),
                                  child: new Text(
                                    'Pickup Point: ${body[index]['PICK_UP_POINT']}',
                                    style: new TextStyle(
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  margin: EdgeInsets.all(5.0),
                                  child: new Text(
                                    'Destination: ${body[index]['DESTINATION']}',
                                    style: new TextStyle(
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  margin: EdgeInsets.all(5.0),
                                  child: new Text(
                                    'Number of passengers: ${body[index]['NO_OF_PASSENGERS']}',
                                    style: new TextStyle(
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  margin: EdgeInsets.all(5.0),
                                  child: new Text(
                                    'Price per seat: ${body[index]['PRICE_PER_SEAT']}',
                                    style: new TextStyle(
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  margin: EdgeInsets.all(5.0),
                                  child: new Text(
                                    'Pickup Point: ${body[index]['VEHICLE_TYPE']}',
                                    style: new TextStyle(
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                                new Padding(
                                  padding: EdgeInsets.only(bottom: 10.0),
                                ),
                              ],
                            ),
                          ],
                        )),
                      ),
                    );
                  }),
            ),
          ),
        );
      }
    } catch (e) {
      return new Container(child: new Text('Nothing to show!'));
    }
  }

  Widget rentalpage(Map<String, dynamic> body, int count) {
    AssetImage top;
    Color backg;
    try {
      if (count == 0) {
        return new RefreshIndicator(
          onRefresh: () async {
            GetRent gr = new GetRent();
            await gr.getrents();
            setState(() {
              _bodyrents;
              _rentalloading;
              _rentslength;
            });
            return Future.delayed(new Duration(milliseconds: 1000));
          },
          child: new Center(
            child: new ListView(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 2.8),
              children: <Widget>[new Center(child: Text('No data'))],
            ),
          ),
        );
      } else {
        return new Container(
            margin: EdgeInsets.all(10.0),
            child: new RefreshIndicator(
              onRefresh: () async {
                GetRent gr = new GetRent();
                await gr.getrents();
                setState(() {
                  _bodyrents;
                  _rentalloading;
                  _rentslength;
                });
                return Future.delayed(new Duration(milliseconds: 1000));
              },
              child: new ListView.builder(
                itemCount: count == null ? 0 : count,
                itemBuilder: (BuildContext context, int index) {
                  if (body['rentals'][index]['SERVICE_TYPE'].contains('en')) {
                    top = car;
                    backg = Colors.tealAccent;
                  } else if (body['rentals'][index]['SERVICE_TYPE']
                      .contains('aul')) {
                    top = truck;
                    backg = Colors.yellowAccent;
                    ;
                  } else {
                    top = new AssetImage('assets/calendar.ico');
                    backg = Colors.lightGreenAccent;
                  }

                  return new GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('/tripinfo');
                    },
                    child: new Card(
                      //    color: Colors.orange[100],
                      elevation: 9.0,
                      child: new ListTile(
                        onLongPress: () async {
                          const url = 'https://gmail.com';
                          if (await canLaunch(url)) {
                            await launch(url);
                          } else {
                            throw 'could not launch';
                          }
                        },
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Payscreen(
                                      price: '0.20',
                                      description: 'Paying for a Rent',
                                      clientref:
                                          '${body['rentals'][index]['CLIENTREF']}',
                                      paymentType: 'rent',
                                      item:
                                          '${body['rentals'][index]['PICK_UP_POINT']} - ${body['rentals'][index]['DESTINATION']}',
                                    )),
                          );
                        },
                        leading: new Image(
                          image: top,
                          color: Colors.orange,
                          height: 35.0,
                        ),
                        trailing: body['rentals'][index]['PAID']
                            ? new Container(
                                child: new Text(
                                  'Paid',
                                  style: new TextStyle(color: Colors.orange),
                                ),
                              )
                            : new Container(
                                child: new Text(
                                  'Pending',
                                  style: new TextStyle(color: Colors.orange),
                                ),
                              ),
                        title: new Text(
                          '${body['rentals'][index]['PICK_UP_POINT']} - ${body['rentals'][index]['DESTINATION']}',
                          style: new TextStyle(
                              color: Colors.orange,
                              fontWeight: FontWeight.bold),
                        ),
                        subtitle: new Text(
                          'End: ${body['rentals'][index]['END']}\nStart Date: ${body['rentals'][index]['START']}',
                          style: new TextStyle(fontStyle: FontStyle.italic),
                        ),
                        isThreeLine: true,
                      ),
                    ),
                  );
                },
              ),
            ));
      }
    } catch (e) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    }
  }

  var car = AssetImage('assets/sedan.ico');
  var haullogo = AssetImage('assets/truck.jpg');
  var eventlogo = AssetImage('assets/event.jpg');

  TabController tabController =
      new TabController(vsync: AnimatedListState(), length: 3, initialIndex: 0);

  @override
  void initState() {
    GetRent getRent = new GetRent();
    GetOrganised getOrganised = new GetOrganised();
    GetTrip getTrip = new GetTrip();
    setState(() {
      getOrganised.getorganised();
      getTrip.gettrips();
      getRent.getrents();
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          bottom: new TabBar(
            controller: tabController,
            tabs: <Widget>[
              new Container(
                child: new Tab(
                  child: new Text(
                    'Trips',
                    style: new TextStyle(color: Colors.black),
                  ),
                ),
              ),
              new Container(
                child: new Tab(
                  child: new Text(
                    'Rentals',
                    style: new TextStyle(color: Colors.black),
                  ),
                ),
              ),
              new Container(
                child: new Tab(
                  child: new Text(
                    'Organized',
                    style: new TextStyle(color: Colors.black),
                  ),
                ),
              ),
              // new Container(
              //   child: new Tab(
              //     child: new Text(
              //       'Notification',
              //       style: new TextStyle(color: Colors.black),
              //     ),
              //   ),
              // ),
            ],
          ),
          title: new Text(
            'Dashboard',
            style: new TextStyle(color: Colors.black),
            textAlign: TextAlign.center,
          ),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.account_circle),
              onPressed: () {
                Navigator.of(context).pushNamed('/profilepage');
              },
            ),
            new IconButton(
              tooltip: 'Refund',
              icon: new Icon(Icons.account_balance_wallet),
              color: Colors.black,
              onPressed: () {
                var alert = new AlertDialog(
                  title: new Text(
                    "How To Get A Refund",
                    textAlign: TextAlign.center,
                  ),
                  content: new Text(
                      "Call support line +233 24 358 4253 OR send a mail to info@rydelogistics.com with details of your refund request i.e. Date, Booking/Reference No., Amount.",
                      textAlign: TextAlign.center),
                );
                showDialog(context: context, child: alert);
              },
            ),
            new IconButton(
              tooltip: 'About Us',
              icon: new Icon(Icons.info_outline),
              color: Colors.black,
              onPressed: () {
                Navigator.of(context).pushNamed('/aboutpage');
              },
            ),
            new IconButton(
              tooltip: 'Logout',
              icon: new Icon(Icons.exit_to_app),
              color: Colors.black,
              onPressed: () async {
                Future<SharedPreferences> checklogin =
                    SharedPreferences.getInstance();

                Future<Null> storelogin() async {
                  final SharedPreferences loginpref = await checklogin;
                  loginpref.setString('token', '');
                }

                await storelogin();
                Navigator.of(context).pushReplacementNamed('/login');
              },
            ),
          ],
        ),
        body: new TabBarView(
          controller: tabController,
          children: <Widget>[
            new Container(
              child: _triploading
                  ? new Center(child: new CircularProgressIndicator())
                  // : tripstab(
                  //     bodytrips == null ? [] : bodytrips, tripslength, car),
                  : tripstab(_bodytrips['trips'], _tripslength, car),
              //This is Temporary Remeber TODO
            ),
            new Container(
              child: _rentalloading
                  ? new Center(
                      child: new CircularProgressIndicator(),
                    )
                  : rentalpage(_bodyrents, _rentslength),
            ),
            new Container(
              child: _orgloading
                  ? new Center(child: new CircularProgressIndicator())
                  : orgpage(_bodyorg['data'], _orglength),
            ),
          ],
        ));
  }
}

class GetOrganised {
  getorganised() async {
    var client = new http.Client();
    var response = await client
        .get('https://ryde-rest.herokuapp.com/Userorganized', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    });
    _bodyorg = await json.decode(response.body);
    _orglength = _bodyorg['data'].length;
    _orgloading = false;
  }
}

class GetRent {
  getrents() async {
    var client = new http.Client();
    var response = await client
        .get('https://ryde-rest.herokuapp.com/userRentals', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token
    });
    _bodyrents = await json.decode(response.body);
    _rentslength = _bodyrents['rentals'].length;
    _rentalloading = false;
  }
}

class GetTrip {
  gettrips() async {
    try {
      var client = new http.Client();
      var response = await client
          .get('https://ryde-rest.herokuapp.com/userTrips', headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': token,
      });
      _bodytrips = await json.decode(response.body);
      _tripslength = _bodytrips['trips'].length;
      _triploading = false;
      print(_bodytrips);
    } catch (e) {
      _tripslength = 0;
    }
  }
}

class GetPassengers {
  getjoined() async {
    try {
      var client = new http.Client();
      var response = await client
          .get('https://ryde-rest.herokuapp.com/joinedOrganized', headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': token,
      });
      print(json.decode(response.body));
    } catch (e) {}
  }
}
