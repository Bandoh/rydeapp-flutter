import 'package:flutter/material.dart';
import 'package:rydeapp/customWidgets.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  String ryderental =
      'This service covers rental requests of the following vehicles - saloon cars, four wheel drives and vans for basic rental requests as well as shuttle services (employee pickups; bus shuttles for schools, child pickups etc).';
  String rydehaulage =
      'This service includes the rental of trucks, delivery vans and pickups for your everyday cargo and delivery activities';
  String event =
      'Create an event or trip and allow people to pay towards it. Each created trip or event comes with a live report to keep you up-to-date on payments.';
  String content =
      'Everyone needs mobility at one time or the other for one reason or the other. Want to get the best deal? Talk to us. It’s time you took a Ryde.';
  var car = AssetImage('assets/file.jpg');
  var haullogo = AssetImage('assets/truck.jpg');
  var eventlogo = AssetImage('assets/event.jpg');
  var backg = AssetImage('assets/banner1.jpg');
  var bus = new AssetImage('assets/bus.png');

  double toppadding = 20.0;
  double bottompadding = 10.0;

  @override
  Widget build(BuildContext context) {
  double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
        //backgroundColor: Colors.lightBlue[100] ,
      appBar: appbar('Services', context, '/aboutpage'),
      body: new Container(
        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: new Center(
          child: new ListView(
            children: <Widget>[
              new Card(
                elevation: 10.0,
                child: new Container(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: backg,
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: new Container(
                    decoration: new BoxDecoration(
                      color: Colors.orange.withOpacity(0.2),
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 70.0,
                          margin: EdgeInsets.only(
                              top: 10.0,bottom: 10.0),
                          child: new Image(
                            image: bus,
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(
                              top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                          child: new Text(
                            'Ryde Logistics',
                            style: new TextStyle(
                                fontSize: 30.0, color: Colors.white),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(
                              top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                          child: new Text(
                            content,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontSize: 20.0, color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  )),
              ),
              rentalcard('Ryde Rental', ryderental, car, context, '/rentform',screenwidth),
              rentalcard('Ryde Organiser', event, eventlogo, context, '/createevent',screenwidth),
            ],
          ),
        ),
      ),
    );
  }
}
