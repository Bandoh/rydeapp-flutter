import 'dart:async';

import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Interfaces/aboutpage.dart';
import 'package:rydeapp/Interfaces/profilepage.dart';
import 'package:rydeapp/Interfaces/ryderental.dart';
import 'package:rydeapp/Interfaces/home.dart';
import 'package:rydeapp/Interfaces/login.dart';
import 'package:rydeapp/Interfaces/splash.dart';
import 'package:rydeapp/Interfaces/tripspage.dart';
import 'Interfaces/register.dart';
import 'package:rydeapp/Interfaces/payscreen.dart';
import 'package:rydeapp/Interfaces/createevent.dart';
import 'package:rydeapp/Interfaces/registersplash.dart';



double screenwidth = 0.0;
double screenheight = 0.0;
int userid;
String token;
var user;

var apikey = 'AIzaSyA5gm3DbJax7-gDIa7PEsZFrLzFXffUweA';
void main() {
  MapView.setApiKey(apikey);

  Timer(new Duration(microseconds: 5000), () {
  });
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/register': (BuildContext context) => new Register(),
        '/home': (BuildContext context) => new Home(),
        '/rentform': (BuildContext context) => new Rentalform(),
        '/login': (BuildContext context) => new Login(),
        '/tripinfo': (BuildContext context) => new Trips(),
        '/paypage': (BuildContext context) => new Payscreen(),
        '/createevent': (BuildContext context) => new CreateEvent(),
        '/aboutpage': (BuildContext context) => new AboutPage(),
        '/registersplash': (BuildContext context) => new RegisterSplash(),
        '/profilepage': (BuildContext context) => new ProfilePage(),
      },
      title: 'RYDE',
      theme: new ThemeData(
        primarySwatch: Colors.orange,
        primaryColor: Colors.orange,
        accentColor: Colors.orangeAccent,
      ),
      home: new MyHomePage(title: 'Welcome to RYDE!'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Splash(),
    );
  }
}
