import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  bool _checking = false;
  String _token;
  Animation _animation;
AnimationController _control;

Duration timetoleave = new Duration(milliseconds: 2000);
Future<SharedPreferences> checklogin = SharedPreferences.getInstance();

var client = new http.Client();
  Future<String> readlogin() async {
    final SharedPreferences loginpref = await checklogin;
    _token = loginpref.getString('token');
    return _token;
  }

readingLogin() async {
  setState(() {
      _checking = true;
    });
  
    var sToken = await readlogin();
    if (sToken != null) {
      var response = await client
          .get('https://ryde-rest.herokuapp.com/getUser', headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': sToken
      });
      Map<String, dynamic> body = json.decode(response.body);
      if (body['status'] == 'success') {
        print('success');
        token = sToken;
        user = body;
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        Navigator.pushReplacementNamed(context, '/login');
      }
    }
    else if (sToken == null){
      Navigator.pushReplacementNamed(context, '/login');
    }
  }
var logo = new AssetImage('assets/rrydelogo1.jpg');

@override
void initState() {
    super.initState();
    _control = new AnimationController(duration: new Duration(milliseconds: 4000),vsync: this);
    _animation = new CurvedAnimation(parent: _control,curve: Curves.easeInOut);
    _control.addListener((){
      this.setState((){

      });
    });
    _control.forward();
    readingLogin();
   // Timer(Duration(milliseconds: 4000),()=>Navigator.of(context).pushReplacementNamed('/login'));
  }

  @override
  void dispose() {
      _control.dispose();
      super.dispose();
    }


  @override
  Widget build(BuildContext context) {
    return Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new Image(
              image: logo,
              height:5+ _animation.value*250,
            ),
          ),
          new Container(
            child: new Text(
              'Ride with Ryde!',
              style: new TextStyle(
                fontSize: 30.0,
                color: Colors.orange
              ),
            ),
          ),
          new Padding(
             padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height/9
             ),
          ),
          _checking?new Center( child: new CircularProgressIndicator(),): new Container()
        ],
      ),
    );
  }
}
