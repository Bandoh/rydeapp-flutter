import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';
import 'package:url_launcher/url_launcher.dart';

List<String> steps = [
  '1. First Click the PAY NOW button',
  '2. Select either mobile payment/credit card union',
  '3. If you select credit card option, fill your credit card details',
  '4. If you select mobile money payment/select your wallet(MTN mobile money, Vodafone cash, Airtel money,Tigo cash)'
      '5. Now fill the details and make payment',
  '6. Kindly make sure not to delay with payment methods, as token might expire',
  'Hint: If you are on MTN mobile money, kindly allow CASH OUT first'
];

class Payscreen extends StatefulWidget {
  final String price;

  final String clientref;

  final String description;

  final String duration;

  final String item;

  final String paymentType;

  final String tripid;

  final String seatN;

  Payscreen(
      {this.price,
      this.clientref,
      this.item,
      this.description,
      this.paymentType,
      this.tripid,
      this.duration,
      this.seatN});
  @override
  _Payscreen createState() => new _Payscreen();
}

class _Payscreen extends State<Payscreen> {
  bool isloading = false;

  final key = new GlobalKey<ScaffoldState>();
  var backg = new AssetImage('assets/sims.jpg');
  var payimage = new AssetImage('assets/paybutton.png');

  double leftpadding = 10.0;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: key,
        appBar: appbar('Make Payment', context, '/aboutpage'),
        body: Builder(
          builder: (context) {
            return new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: backg,
                  fit: BoxFit.cover,
                ),
              ),
              //margin: EdgeInsets.all(10.0),
              child: new Container(
                height: MediaQuery.of(context).size.height,
                decoration: new BoxDecoration(
                  color: Colors.orange.withOpacity(0.4),
                ),
                child: new ListView(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.only(top: 10.0),
                      child: new Text('Steps For Making Payment',
                          style: new TextStyle(
                              fontSize: 20.0, color: Colors.white)),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: payimage,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      height: 60.0,
                    ),
                    new Container(
                        child: new Card(
                            margin: EdgeInsets.all(10.0),
                            color: Colors.white.withOpacity(0.9),
                            elevation: 10.0,
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                step(steps[0]),
                                step(steps[1]),
                                step(steps[2]),
                                step(steps[3]),
                                step(steps[4]),
                                step(steps[5]),
                                new Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                //step(steps[6]),
                                new Container(
                                  //  flex:1 ,
                                  child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height *
                                        0.08,
                                    child: isloading
                                        ? new Center(
                                            child: CircularProgressIndicator(),
                                          )
                                        : new RaisedButton(
                                            onPressed: () async {
                                              PostPrice postPrice =
                                                  new PostPrice(
                                                      seatN: widget.seatN,
                                                      description:
                                                          widget.description,
                                                      duration: widget.duration,
                                                      clientref:
                                                          widget.clientref,
                                                      item: widget.item,
                                                      paymentType:
                                                          widget.paymentType,
                                                      price: widget.price,
                                                      tripid: widget.tripid);

                                              setState(() {
                                                isloading = true;
                                                print(isloading);
                                              });

                                              await postPrice.postprice(
                                                  context, isloading);

                                              setState(() {
                                                isloading = false;
                                              });
                                            },
                                            child: new Text('PAY NOW'),
                                            color: Colors.orangeAccent,
                                          ),
                                  ),
                                ),
                              ],
                            ))),
                  ],
                ),
              ),
            );
          },
        ));
  }

  Widget step(String t) {
    return new Container(
      margin: EdgeInsets.only(
        top: 8.0,
        left: 10.0,
        right: 10.0,
        bottom: 2.0,
      ),
      child: new Text(
        t,
        style: new TextStyle(
          fontSize: 15.0,
          color: Colors.black,
          //fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class PostPrice {
  String price;

  String clientref;

  String description;

  String duration;

  String item;

  String paymentType;

  String seatN;

  String tripid;
  PostPrice(
      {this.price,
      this.clientref,
      this.item,
      this.description,
      this.paymentType,
      this.tripid,
      this.duration,
      this.seatN});
  var client = new http.Client();
  Future postprice(BuildContext context, bool isLoading) async {
    isLoading = true;
    if (this.price.contains('200')) {
      this.price = '200.00';
      this.duration = 'monthly';
    } else if (this.price.contains('40')) {
      this.price = '40.00';
      this.duration = 'twoweeks';
    } else if (this.price.contains('3')) {
      this.price = '3.00';
      this.duration = 'threedays';
    } else {
      this.price = '20.00';
      this.duration = 'weekly';
    }
    print(this.clientref);
    print(this.item);
    print(this.description);
    print(this.paymentType);
    print(this.price);
    var r =
        await client.post('https://ryde-rest.herokuapp.com/checkout', headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      'x-access-token': token,
    }, body: {
      "item": this.item.toString(),
      "price": this.price.toString(),
      'description': this.description,
      'clientReference': this.clientref == null ? '' : this.clientref,
      'payment_type': this.paymentType,
      'duration': this.duration == null ? '' : this.duration,
      'tripid': this.tripid == null ? '' : this.tripid,
      'seatNo': this.seatN == null ? '' : this.seatN
    }).then((res) {
      isLoading = false;
      print(res.body);
      Map<String, dynamic> body = json.decode(res.body);
      if (body['status'] == 'success') {
        GetRent getRent = new GetRent();
        GetOrganised getOrganised = new GetOrganised();
        GetTrip getTrip = new GetTrip();

        getOrganised.getorganised();
        getTrip.gettrips();
        getRent.getrents();
        launch(body['checkoutUrl']);
      } else if (body['status'] == 'failed') {
        final snackBar = SnackBar(
          duration: new Duration(milliseconds: 7000),
          content: Text(body['checkoutUrl']['data']),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      }
    });
  }
}
