import 'package:flutter/material.dart';
import 'package:rydeapp/customWidgets.dart';

class Trips extends StatefulWidget {
  @override
  _TripsState createState() => _TripsState();
}

class _TripsState extends State<Trips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbar('Trips',context,'/aboutpage'),
      body: new Center(
          child: new Container(
            height: 500.0,
        margin: EdgeInsets.all(20.0),
        decoration: new BoxDecoration(border: Border.all(color: Colors.black)),
        child: new Column(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.orange, width: 2.0))),
              margin: EdgeInsets.all(10.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: new Text(
                      'Event',
                      style: new TextStyle(
                          color: Colors.orange,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.all(5.0),
                    child: new Text(
                      '27-20-17',
                      style: new TextStyle(color: Colors.orange),
                    ),
                  ),
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.all(10.0),
              child: new Text('Status : Pending'),
            ),
            tripsforminfo('Event Name: ', 'Tidal Rave'),
            tripsforminfo('Email: ', 'itsme@yahoo.com'),
            tripsforminfo('Phone Number: ', '+233 3780543'),
            tripsforminfo('Number of Seats: ', '5'),
            tripsforminfo('Number of Vehicles: ', '2'),
            tripsforminfo('Confirm from Ryde: ', 'Yes'),
            tripsforminfo('From: ', 'Accra'),
            tripsforminfo('To: ', 'Circle'),
            tripsforminfo('Price per seat: ', 'GHS 1.00'),
          ],
        ),
      )),
    );
  }
}
