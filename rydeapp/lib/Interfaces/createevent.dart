import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/main.dart';
import 'package:http/http.dart' as http;

var client = new http.Client();

class CreateEvent extends StatefulWidget {
  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent> {
  final formKey = GlobalKey<FormState>();
  var logo = new AssetImage('assets/rrydelogo1.jpg');
  double leftpadding = 25.0;
  double topdownpadding = 10.0;

  TextEditingController starttime = new TextEditingController();
  TextEditingController endtime = new TextEditingController();
  TextEditingController passengers = new TextEditingController();
  TextEditingController seats = new TextEditingController();
  TextEditingController zone = new TextEditingController();
  TextEditingController pickup = new TextEditingController();
  TextEditingController _destination = new TextEditingController();
  TextEditingController vehicles = new TextEditingController();
  int i = 0;
  String from;
  String to;
  String inicar;
  String iniserv;
  int inidur;

  bool isloading = false;

  bool _enabled = false;

  @override
  void initState() {
    // TODO: implement initState
    from = places.elementAt(0);
    to = places.elementAt(0);
    inicar = cars.elementAt(0);
    iniserv = cars.elementAt(0);
    inidur = rentduration.elementAt(0);
    super.initState();
  }

  void _onchangecar(String value) {
    setState(() {
      inicar = value;
      print(inicar);
    });
  }

  void _onchangedur(int value) {
    setState(() {
      inidur = value;
    });
  }

  var backg = new AssetImage('assets/banner2.jpg');
  var bus = new AssetImage('assets/bus.png');
  void postForm() {
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate()) {
      client.post('https://ryde-rest.herokuapp.com/organize', headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "x-access-token": token
      }, body: {
        'destination': _destination.text.trim(),
        'pick_up_point': pickup.text.trim(),
        'vehicle_type': inicar.toString(),
        'passengers': passengers.text.trim(),
        'seats': seats.text.trim(),
        'ryde_vehicle': _enabled.toString(),
        'vehicles': vehicles.text.trim(),
        'tripName': 'www',
      }).then(
        (response) {
          Map<String, dynamic> body = json.decode(response.body);
          print(body['organizer']);
          setState(() {
            isloading = false;
          });
          if (body['status'] == 'success') {
            var err = new AlertDialog(
                title: new Center(
                    child: new Container(
                        color: Colors.orangeAccent,
                        child: new Center(
                            child: new Icon(
                          Icons.mail_outline,
                          color: Colors.white,
                          size: 120.0,
                        )))),
                content: new Container(
                  height: 100.0,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          child: new Text(
                        'Mail sent to ${body['organizer']['EMAIL']}',
                        textAlign: TextAlign.center,
                        softWrap: true,
                      )),
                      new Container(
                        child: new FlatButton(
                          //color: Colors.orangeAccent,
                          child: new Text(
                            'Ok',
                            style: new TextStyle(
                                color: Colors.orangeAccent, fontSize: 20.0),
                          ),
                          onPressed: () {
                            // Navigator.pushReplacementNamed(context,'/home');
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ));
            showDialog(context: context, child: err);
            GetOrganised go = new GetOrganised();
            go.getorganised();
          } else {}
        },
      );
    } else {
      setState(() {
        isloading = false;
      });
    }
  }

  final timeFormat = DateFormat("h:mm");

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      backgroundColor: Colors.teal,
      appBar: appbar('Organiser', context, '/aboutpage'),
      body: Container(
        child: new Container(
            child: new SingleChildScrollView(
                child: new Container(
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.06,
              bottom: 0.0,
              right: 10.0,
              left: 10.0),
          child: new Card(
            elevation: 30.0,
            child: new Form(
              key: formKey,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    child: new Image(
                      image: logo,
                      height: 0.1 * MediaQuery.of(context).size.height,
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                      left: leftpadding,
                      right: leftpadding * 3,
                      //bottom: leftpadding
                    ),
                    child: new TextFormField(
                      validator: (val) =>
                          val.isEmpty ? 'Please Input Pickup point' : null,
                      controller: pickup,
                      decoration: new InputDecoration(
                          labelText: 'Pickup point: ',
                          labelStyle: new TextStyle(color: Colors.orange)),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                      left: leftpadding,
                      right: leftpadding * 3,
                      //bottom: leftpadding
                    ),
                    child: new TextFormField(
                      validator: (val) =>
                          val.isEmpty ? 'Please Input Destination' : null,
                      controller: _destination,
                      decoration: new InputDecoration(
                          labelText: 'Destination: ',
                          labelStyle: new TextStyle(color: Colors.orange)),
                    ),
                  ),
                  // new Row(
                  //   children: <Widget>[
                  // new Container(
                  //   width: screenwidth * 0.23,
                  //   margin: EdgeInsets.only(
                  //       left: leftpadding, top: topdownpadding),
                  //   child: new Text(
                  //     'pickup point :',
                  //     style: new TextStyle(fontSize: 15.0),
                  //   ),
                  // ),
                  // new Center(
                  //   child: new Container(
                  //       margin: EdgeInsets.only(
                  //           left: leftpadding, top: topdownpadding),
                  //       child: new DropdownButton(
                  //         onChanged: (dynamic value) {
                  //           _onchangeto(value);
                  //         },
                  //         value: to,
                  //         items: places.map((String value) {
                  //           return new DropdownMenuItem(
                  //             value: value,
                  //             child: new Container(
                  //               width: screenwidth * 0.5,
                  //               child: new Text(value),
                  //             ),
                  //           );
                  //         }).toList(),
                  //       )),
                  // ),
                  //   ],
                  // ),
                  // new Row(children: <Widget>[
                  //   new Container(
                  //     width: screenwidth * 0.23,
                  //     margin: EdgeInsets.only(
                  //         left: leftpadding, top: topdownpadding),
                  //     child: new Text(
                  //       'Destination :',
                  //       style: new TextStyle(fontSize: 15.0),
                  //     ),
                  //   ),
                  //   new Center(
                  //     child: new Container(
                  //         margin: EdgeInsets.only(
                  //             left: leftpadding, top: topdownpadding),
                  //         // width: 400.0,
                  //         //margin: EdgeInsets.only(),
                  //         child: new DropdownButton(
                  //           onChanged: (dynamic value) {
                  //             _onchangefrom(value);
                  //           },
                  //           value: from,
                  //           items: places.map((String value) {
                  //             return new DropdownMenuItem(
                  //               value: value,
                  //               child: new Container(
                  //                 width: screenwidth * 0.5,
                  //                 child: new Text(value),
                  //               ),
                  //             );
                  //           }).toList(),
                  //         )),
                  //   ),
                  // ]),
                  new Row(children: <Widget>[
                    new Container(
                      //width: screenwidth * 0.23,
                      margin: EdgeInsets.only(
                          left: leftpadding, top: topdownpadding),
                      child: new Text(
                        'Vehicle Type :',
                        style:
                            new TextStyle(fontSize: 15.0, color: Colors.orange),
                      ),
                    ),
                    new Center(
                      child: new Container(
                          margin: EdgeInsets.only(
                              left: leftpadding / 5, top: topdownpadding),
                          // width: 400.0,
                          //margin: EdgeInsets.only(),
                          child: new DropdownButton(
                            onChanged: (dynamic value) {
                              _onchangecar(value);
                            },
                            value: inicar,
                            items: cars.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Container(
                                  //width: screenwidth * 0.5,
                                  child: new Text(value),
                                ),
                              );
                            }).toList(),
                          )),
                    ),
                  ]),
                  // new Container(
                  //   margin: EdgeInsets.only(
                  //       left: leftpadding, right: leftpadding * 3),
                  //   child: TimePickerFormField(
                  //     validator: (val) => val == null
                  //         ? 'Cannot leave Start time blank'
                  //         : null,
                  //     initialTime: TimeOfDay.now(),
                  //     controller: starttime,
                  //     enabled: true,
                  //     decoration: new InputDecoration(
                  //         labelText: 'Start Time',
                  //         labelStyle:
                  //             new TextStyle(color: Colors.orange)),
                  //     format: timeFormat,
                  //     onChanged: (time) {
                  //       //print('$date');
                  //     },
                  //   ),
                  // ),
                  // new Container(
                  //   margin: EdgeInsets.only(
                  //     left: leftpadding,
                  //     right: leftpadding * 3,
                  //     // bottom: leftpadding
                  //   ),
                  //   child: TimePickerFormField(
                  //     validator: (val) => val == null
                  //         ? 'Cannot leave End time blank'
                  //         : null,
                  //     initialTime: TimeOfDay.now(),
                  //     controller: endtime,
                  //     enabled: true,
                  //     decoration: new InputDecoration(
                  //         hintText: '${TimeOfDay.now()}',
                  //         labelText: 'End Time',
                  //         labelStyle:
                  //             new TextStyle(color: Colors.orange)),
                  //     format: timeFormat,
                  //     onChanged: (time) {
                  //       print('This is end time ${endtime.text}');
                  //       // print('$date');
                  //     },
                  //   ),
                  // ),
                  // new Container(
                  //   margin: EdgeInsets.only(
                  //     left: leftpadding,
                  //     right: leftpadding * 3,
                  //     // bottom: leftpadding
                  //   ),
                  //   child: new TextFormField(
                  //     validator: (val) =>
                  //         val.isEmpty ? 'Please Input Zone' : null,
                  //     controller: zone,
                  //     decoration: new InputDecoration(
                  //         labelText: 'Zone: ',
                  //         labelStyle:
                  //             new TextStyle(color: Colors.orange)),
                  //   ),
                  // ),
                  // new Container(
                  //   margin: EdgeInsets.only(bottom: leftpadding * 3),
                  //   child: new Row(
                  //     children: <Widget>[
                  //       new Container(
                  //         width: 130.0,
                  //         margin: EdgeInsets.only(
                  //           left: leftpadding,
                  //           //right: leftpadding * 3,
                  //           //bottom: leftpadding
                  //         ),
                  //         child: new TextFormField(
                  //           validator: (val) => val.isEmpty
                  //               ? 'Please Input price'
                  //               : null,
                  //           keyboardType: TextInputType.number,
                  //           controller: weekly,
                  //           decoration: new InputDecoration(
                  //               hintText: 'eg. 2.00',
                  //               labelText: 'Weekly Price: ',
                  //               labelStyle: new TextStyle(
                  //                   color: Colors.orange)),
                  //         ),
                  //       ),
                  //       new Container(
                  //         width: 130.0,
                  //         margin: EdgeInsets.only(
                  //           left: leftpadding,
                  //           //  right: leftpadding * 3,
                  //           //bottom: leftpadding
                  //         ),
                  //         child: new TextFormField(
                  //           validator: (val) => val.isEmpty
                  //               ? 'Please Input Price'
                  //               : null,
                  //           keyboardType: TextInputType.number,
                  //           controller: monthly,
                  //           decoration: new InputDecoration(
                  //               hintText: 'eg. 2.00',
                  //               labelText: 'Monthly Price: ',
                  //               labelStyle: new TextStyle(
                  //                   color: Colors.orange)),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  new Container(
                    margin: EdgeInsets.only(bottom: topdownpadding),
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          width: 130.0,
                          margin: EdgeInsets.only(
                            left: leftpadding,
                            //right: leftpadding * 3,
                            //bottom: leftpadding
                          ),
                          child: new TextFormField(
                            validator: (val) =>
                                val.isEmpty ? 'Passengers required' : null,
                            keyboardType: TextInputType.number,
                            controller: passengers,
                            decoration: new InputDecoration(
                                hintText: 'eg. 50',
                                labelText: 'Passengers: ',
                                labelStyle:
                                    new TextStyle(color: Colors.orange)),
                          ),
                        ),
                        new Container(
                          width: 130.0,
                          margin: EdgeInsets.only(
                            left: leftpadding,
                            //  right: leftpadding * 3,
                            //bottom: leftpadding
                          ),
                          child: new TextFormField(
                            validator: (val) =>
                                val.isEmpty ? 'Seat price required' : null,
                            keyboardType: TextInputType.number,
                            controller: seats,
                            decoration: new InputDecoration(
                                hintText: 'eg. 70.00',
                                labelText: 'Seat Price: ',
                                labelStyle:
                                    new TextStyle(color: Colors.orange)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    child: new Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(),
                          child: new Row(children: <Widget>[
                            new Container(
                              width: 130.0,
                              margin: EdgeInsets.only(
                                left: leftpadding,
                                //right: leftpadding * 3,
                                bottom: topdownpadding,
                              ),
                              child: new TextFormField(
                                validator: (val) =>
                                    val.isEmpty ? 'Vehicles Required' : null,
                                keyboardType: TextInputType.number,
                                controller: vehicles,
                                decoration: new InputDecoration(
                                    hintText: 'eg. 5',
                                    labelText: 'Vehicles: ',
                                    labelStyle:
                                        new TextStyle(color: Colors.orange)),
                              ),
                            ),
                            new Container(
                              //width: screenwidth * 0.23,
                              margin: EdgeInsets.only(
                                top: topdownpadding * 3.5,
                                left: leftpadding * 1.2,
                              ),
                              child: new Text(
                                'Duration :',
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.orange),
                              ),
                            ),
                            new Center(
                              child: new Container(
                                  margin: EdgeInsets.only(
                                      left: leftpadding / 2,
                                      top: topdownpadding * 3),
                                  // width: 400.0,
                                  //margin: EdgeInsets.only(),
                                  child: new DropdownButton(
                                    isDense: true,
                                    //hint: new Text('Duration'),
                                    onChanged: (dynamic value) {
                                      _onchangedur(value);
                                    },
                                    value: inidur,
                                    items: rentduration.map((int value) {
                                      return new DropdownMenuItem(
                                        value: value,
                                        child: new Container(
                                          width: screenwidth / 20,
                                          child: new Text(value.toString()),
                                        ),
                                      );
                                    }).toList(),
                                  )),
                            ),
                          ]),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                      margin: EdgeInsets.only(
                          bottom: topdownpadding * 3, top: topdownpadding),
                      child: new Row(
                        children: <Widget>[
                          new Container(
                              width: screenwidth * 0.23,
                              margin: EdgeInsets.only(
                                  left: leftpadding, top: topdownpadding),
                              child: new Text(
                                'Ryde Vehicle',
                                style: new TextStyle(color: Colors.orange),
                              )),
                          new Container(
                            margin: EdgeInsets.only(left: leftpadding * 0.25),
                            child: new Switch(
                              value: _enabled,
                              onChanged: (bool value) {
                                setState(() {
                                  _enabled = value;
                                });
                              },
                            ),
                          ),
                        ],
                      )),
                  !isloading
                      ? new Container(
                          width: 700.0,
                          height: 50.0,
                          margin: EdgeInsets.only(
                            top: 20.0,
                          ),
                          child: new RaisedButton(
                            elevation: 5.0,
                            color: Colors.orangeAccent,
                            onPressed: () {
                              postForm();
                            },
                            child: new Text(
                              "Create",
                              style: new TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ))
                      : new CircularProgressIndicator(),
                ],
              ),
            ),
          ),
        ))),
      ),
    );
  }
}
