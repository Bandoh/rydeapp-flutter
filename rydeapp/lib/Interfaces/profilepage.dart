import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rydeapp/main.dart';
import 'package:dio/dio.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  File _fromupload;

  picker() async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _fromupload = file;
    });
  }

  loaduser() async {
    setState(() {
      user;
    });
  }

  @override
  void initState() {
    loaduser();
    super.initState();
  }

  var current = new AssetImage('assets/danfoman.jpg');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbar('Profile', context, '/aboutpage'),
      body: new Container(
          child: new Stack(
        fit: StackFit.loose,
        children: <Widget>[
          new Container(
            child: new Container(
              decoration: new BoxDecoration(
                color: Colors.orange,
              ),
              height: MediaQuery.of(context).size.height / 2,
              child: new Center(
                  child: new GestureDetector(
                onTap: () {
                  AlertDialog c = new AlertDialog(
                      contentPadding: EdgeInsets.all(0.0),
                      content: new Container(
                        child: new Container(
                            child: new GestureDetector(
                          onTap: () {
                            picker();
                          },
                          child: new Image(
                            image: current,
                            fit: BoxFit.cover,
                          ),
                        )),
                      ));

                  showDialog(context: context, child: c);
                },
                child: new CircleAvatar(
                  backgroundImage: _fromupload == null
                      ? current
                      : new FileImage(_fromupload),
                  //new NetworkImage(user['user']['PHOTO']),
                  radius: MediaQuery.of(context).size.width / 4,
                ),
              )),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(
                top: check
                    ? MediaQuery.of(context).size.height / 9
                    : MediaQuery.of(context).size.height / 2.4),
            decoration: new BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                color: Colors.white),
            child: new ListView(
              children: <Widget>[
                new Container(
                  child: new IconButton(
                      color: Colors.orange,
                      icon: new Icon(
                          check ? Icons.arrow_downward : Icons.arrow_upward),
                      onPressed: () {
                        checkclick();
                      }),
                ),
                infos(
                    'Name',
                    user['user']['FULL_NAME'] == null
                        ? ''
                        : user['user']['FULL_NAME']),
                infos('Email',
                    user['user']['EMAIL'] == null ? '' : user['user']['EMAIL']),
                infos(
                    'Phone Number',
                    user['user']['PHONE_NUMBER'] == null
                        ? ''
                        : user['user']['PHONE_NUMBER']),
                new Padding(
                  padding: EdgeInsetsDirectional.only(bottom: 30.0),
                )
              ],
            ),
          ),
        ],
      )),
    );
  }

  Widget infos(String label, String info) {
    return new Container(
      margin: EdgeInsets.only(
        left: MediaQuery.of(context).size.width / 15,
        right: MediaQuery.of(context).size.width / 15,
      ),
      child: new Column(
        children: <Widget>[
          new Container(
              child: new TextFormField(
            initialValue: info,
            decoration: new InputDecoration(
              labelText: label,
            ),
          ))
        ],
      ),
    );
  }

  bool check = false;
  checkclick() {
    setState(() {
      if (check) {
        check = false;
      } else {
        check = true;
      }
    });
    print(check);
  }
}

class PostImg {
  postimg() async {
    Dio dio = new Dio();
    var response;
    FormData formData = new FormData.from(
        {"file1": new UploadFileInfo(new File("./upload.jpg"), "upload1.jpg")});
    response = await dio.post("/info", data: formData);
  }
}
