import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as userloc;
import 'package:rydeapp/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

Map<String, double> userlocation = {};

var client = new http.Client();

TextEditingController email = new TextEditingController();
TextEditingController password = new TextEditingController();

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  userloc.Location _loca = new userloc.Location();
  Future<SharedPreferences> checklogin = SharedPreferences.getInstance();

  Future<Null> storelogin() async {
    final SharedPreferences loginpref = await checklogin;
    loginpref.setString('token', token);
  }

  storinglogin() async {
    await storelogin();
  }

  getuser() async {
    try {
      var l = await _loca.hasPermission();
      userlocation = await _loca.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        print(
            'Permission denied - please ask the user to enable it from the app settings');
      }
      userlocation['longitude'] = -0.2497708;
      userlocation['latitude'] = 5.5912045;
    }
  }

  final formKey = GlobalKey<FormState>();

  bool isloading = false;
  var textcolor = Colors.white;
  var heading = Colors.white;

  double leftandrighdpadding = 10.0;
  double upanddownpadding = 15.0;

  var backg = new AssetImage('assets/reseat.jpg');
  var logo = new AssetImage('assets/rrydelogo1.jpg');

  void postForm(BuildContext context) {
    // if (_token != null) {
    // }
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate()) {
      client.post('https://ryde-rest.herokuapp.com/login', headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      }, body: {
        "email": email.text.toLowerCase().trim(),
        "password": password.text
      }).then(
        (response) {
          Map<String, dynamic> body = json.decode(response.body);
          print(body['token']);
          setState(() { 
            isloading = false;
          });
          if (body['status'] == 'success') {
            user = body;
            print(user['user']['FULL_NAME']);
            userid = body['user']['id'];
            token = body['token'];
            storelogin();
            Navigator.of(context).pushReplacementNamed('/home');
          } else {
            var err = new AlertDialog(
              title: new Text('Invalid'),
              content: new Text(body['message']),
            );
            showDialog(context: context, child: err);
          }
        },
      );
    } else {
      setState(() {
        isloading = false;
      });
    }
  }


  @override
  void initState() {
    getuser();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(image: backg, fit: BoxFit.cover)),
        child: new Container(
          decoration: new BoxDecoration(color: Colors.orange.withOpacity(0.2)),
          child: new ListView(
              //reverse: true,
              //shrinkWrap: true,
              children: <Widget>[
                new Container(
                  child: new Image(
                    image: logo,
                    height: MediaQuery.of(context).size.height/4,
                  ),
                ),
                new Container(
                 margin: EdgeInsets.only(bottom: 0.0, right: 30.0, left: 30.0),
                  child: new Card(
                    elevation: 50.0,
                    child: new Form(
                      key: formKey,
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.only(
                              left: leftandrighdpadding / 2.5,
                              top: upanddownpadding * 2,
                            ),
                            margin: EdgeInsets.only(
                                left: leftandrighdpadding * 2,
                                right: leftandrighdpadding,
                                bottom: upanddownpadding * 2),
                            child: new TextFormField(
                              validator: (val) =>
                                  val.isEmpty || !val.contains('@')
                                      ? 'Please Input Valid Mail'
                                      : null,
                              keyboardType: TextInputType.emailAddress,
                              controller: email,
                              textAlign: TextAlign.center,
                              style: new TextStyle(color: Colors.black),
                              decoration:
                                  new InputDecoration(labelText: "E-mail"),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                              left: leftandrighdpadding / 2.5,
                            ),
                            margin: EdgeInsets.only(
                                left: leftandrighdpadding * 2,
                                right: leftandrighdpadding,
                                //top: upanddownpadding
                                bottom: upanddownpadding),
                            child: new TextFormField(
                              validator: (val) => val.isEmpty
                                  ? 'Password cannot be left blank'
                                  : null,
                              controller: password,
                              textAlign: TextAlign.center,
                              style: new TextStyle(color: Colors.black),
                              //focusNode: new FocusNode(),
                              obscureText: true,
                              decoration: new InputDecoration(
                                labelText: "Password",
                              ),
                            ),
                          ),
                          new Container(
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                  child: new FlatButton(
                                    onPressed: () {
                                      Navigator
                                          .of(context)
                                          .pushNamed('/register');
                                    },
                                    child: new Text(
                                      "Create an account",
                                    ),
                                  ),
                                ),
                                new Container(
                                  child: new FlatButton(
                                    onPressed: () {
                                      ;
                                    },
                                    child: new Text(
                                      "Forgot Password",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          !isloading
                              ? new Container(
                                  width: 700.0,
                                  height: 50.0,
                                  margin: EdgeInsets.only(
                                    top: 20.0,
                                  ),
                                  child: new RaisedButton(
                                    elevation: 5.0,
                                    color: Colors.orangeAccent,
                                    onPressed: () {
                                      postForm(context);
                                    },
                                    child: new Text(
                                      "Login",
                                      style: new TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ))
                              : new CircularProgressIndicator(),
                        ],
                      ),
                    ),
                  ),
                )
              ]),
        ),
      ),
    );
  }
}
