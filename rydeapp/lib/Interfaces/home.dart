import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:rydeapp/Tabs/firstPage.dart';
import 'package:rydeapp/Tabs/secondPage.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/Tabs/fourthPage.dart';
import 'package:rydeapp/main.dart';
import 'package:rydeapp/classes/GoogleDirections.dart';
import 'package:http/http.dart' as http;

GoogleDirections n = new GoogleDirections(apikey);

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

int currenttab = 0;

var busicon = new AssetImage('assets/bus.png');
var signicon = new AssetImage('assets/sign.png');
var phoneicon = new AssetImage('assets/phone.png');
var qnaicon = new AssetImage('assets/question.png');
var mapicon = new AssetImage('assets/map.png');
var backg = new AssetImage('assets/danfoman.jpg');

double iconheight = 40.0;
double iconwidth = 10.0;

Color bottomcolour = Colors.black;

Color notActive = Colors.black;
Location loc;
Color isActive = Colors.orange;
List<bool> checkcolor = [true, false, false, false, false];
List<int> tab = [0, 1, 2, 3, 4];

class _HomeState extends State<Home> {
  String _linkMessage;
  bool _isCreatingLink = false;

  List<Map<String, dynamic>> loc = [];
  Map<String, double> m = {
    'latit': 0.2,
  };

  var g;
  getr() async {
    Getstation getstation = Getstation();
    g = await getstation.getstation();
    for (int i = 0; i < g.length; i++) {
      loc.add({
        'latitude': g[i]['LAT'],
        'longitude': g[i]['LONG'],
        'areatype': g[i]['DEMANDAREA']
      });
    }
    print(loc);
  }

  @override
  void initState() {
  //  _retrieveDynamicLink();
    getr();
    super.initState();
  }

  var holdme;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: holdme = new Stack(
        children: <Widget>[
          new Offstage(
            offstage: currenttab != 0,
            child: new TickerMode(
              enabled: currenttab == 0,
              child: new FirstPage(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 1,
            child: new TickerMode(
              enabled: currenttab == 1,
              child: new SecondPage(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 2,
            child: new TickerMode(
              enabled: currenttab == 2,
              child: new ThirdPage(),
            ),
          ),
          new Offstage(
            offstage: currenttab != 4,
            child: new TickerMode(
              enabled: currenttab == 4,
              child: new FourthPage(),
            ),
          ),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
         fixedColor: Colors.blue,
          currentIndex: currenttab,

          onTap: (int index) {
            setState(() {
              changecolor(index);
            });
          },
          items: <BottomNavigationBarItem>[ 
             
            new BottomNavigationBarItem(
              
              icon: new Image(
                color: (checkcolor[0]) ? isActive : notActive,
                image: busicon,
                height: iconheight / 2,
              ),
              title:
                  new Text("Home", style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                image: signicon,
                color: (checkcolor[1]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("Services",
                  style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                image: phoneicon,
                color: (checkcolor[2]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("Dashboard",
                  style: new TextStyle(color: Colors.black)),
            ),
            new BottomNavigationBarItem(
              icon: new IconButton(
                icon: new Image(
                  color: (checkcolor[3]) ? isActive : notActive,
                  image: mapicon,
                  height: iconheight / 2,
                ),
                onPressed: () {
               //   _createDynamicLink(false);
                  // MapPageClass mapPageClass = new MapPageClass.formarkers(
                  //   number: g.length,
                  //    points: loc,
                  //     latitude: userlocation['latitude'],
                  //     longitude: userlocation['longitude'],
                  //      zoom: 15.0,

                  // );
                  // mapPageClass.showmap();
                },
              ),
              title: new Text("Map"),
            ),
            new BottomNavigationBarItem(
              icon: new Image(
                image: qnaicon,
                color: (checkcolor[4]) ? isActive : notActive,
                height: iconheight / 2,
              ),
              title: new Text("QnA", style: new TextStyle(color: Colors.black)),
            ),
          ]),
    );
  }

  changecolor(int ind) {
    for (int i = 0; i < checkcolor.length; i++) {
      checkcolor[i] = false;
    }
    currenttab = ind;
    checkcolor[ind] = true;
  }

//   Future<void> _createDynamicLink(bool short) async {
//     setState(() {
//       _isCreatingLink = true;
//     });

//     final DynamicLinkParameters parameters = DynamicLinkParameters(
//       domain: 'cx4k7.app.goo.gl',
//       link: Uri.parse('https://dynamic.link.example/helloworld'),
//       androidParameters: AndroidParameters(
//         packageName: 'io.flutter.plugins.firebasedynamiclinksexample',
//         minimumVersion: 0,
//       ),
//       dynamicLinkParametersOptions: DynamicLinkParametersOptions(
//         shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
//       ),
//       iosParameters: IosParameters(
//         bundleId: 'com.google.FirebaseCppDynamicLinksTestApp.dev',
//         minimumVersion: '0',
//       ),
//     );

//     Uri url;
//     if (short) {
//       final ShortDynamicLink shortLink = await parameters.buildShortLink();
//       url = shortLink.shortUrl;
//     } else {
//       url = await parameters.buildUrl();
//     }

//     setState(() {
//       _linkMessage = url.toString();
//       _isCreatingLink = false;
//     });
//   }

//   Future<void> _retrieveDynamicLink() async {
//     final PendingDynamicLinkData data =
//         await FirebaseDynamicLinks.instance.retrieveDynamicLink();
//     final Uri deepLink = data?.link;

//     if (deepLink != null) {
//       Navigator.pushNamed(context, deepLink.path);
//     }
//   }
 }

class Getstation {
  Future<List<dynamic>> getstation() async {
    var client = new http.Client();
    String url = 'https://ryde-rest.herokuapp.com/allstations';
    var response = await client.get(url, headers: {'x-access-token': token});
    Map<String, dynamic> body = json.decode(response.body);
    return body['stations'];
  }
}
