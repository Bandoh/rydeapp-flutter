import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rydeapp/Tabs/thirdPage.dart';
import 'package:rydeapp/customWidgets.dart';
import 'package:rydeapp/Interfaces/dropdownlistitems.dart';
import 'package:rydeapp/classes/DateandTime.dart';
import 'package:http/http.dart' as http;
import 'package:rydeapp/main.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

var client = new http.Client();

DateandTime fit = new DateandTime();
DateandTime fit1 = new DateandTime();

class Rentalform extends StatefulWidget {
  @override
  _RentalformState createState() => new _RentalformState();
}

bool isloading = false;
var backg = new AssetImage('assets/reseat.jpg');
var bus = new AssetImage('assets/bus.png');

String content =
    'Do you have a unique request and need a vehicle to rent or for haulage/delivery services? You are at the right place!';

class _RentalformState extends State<Rentalform> {
  final formKey = GlobalKey<FormState>();
  final dateFormat = DateFormat("MM-d-yyyy");
  var logo = new AssetImage('assets/rrydelogo1.jpg');
  double leftpadding = 25.0;
  double topdownpadding = 10.0;

  TextEditingController startdate = new TextEditingController();
  TextEditingController enddate = new TextEditingController();
  TextEditingController duration = new TextEditingController();
  TextEditingController pickup = new TextEditingController();
  TextEditingController destination = new TextEditingController();
  int _selected = 0;
  int i = 0;
  String from;
  String to;
  String inicar;
  String iniserv;
  int inidur;
  String dur;
  DateTime start;
  DateTime end;

  @override
  void initState() {
    // TODO: implement initState
    from = places.elementAt(0);
    to = places.elementAt(0);
    inicar = cars.elementAt(0);
    iniserv = cars.elementAt(0);
    inidur = rentduration.elementAt(0);
    super.initState();
  }

  List<String> serv = ['Rental', 'Haulage'];

  void postForm() {
    dur = calcDatediff(start, end);
    final form = formKey.currentState;
    setState(() {
      isloading = true;
    });
    if (form.validate()) {
      client.post('https://ryde-rest.herokuapp.com/rent', headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "x-access-token": token
      }, body: {
        "vehicle": inicar,
        "service": serv[_selected],
        'duration': dur,
        'pick_up_point': pickup.text.trim(),
        'destination': destination.text.trim(),
        'start': startdate.text.trim(),
        'end': enddate.text.trim(),
      }).then(
        (response) {
          Map<String, dynamic> body = json.decode(response.body);
          setState(() {
            isloading = false;
          });
          print(body);
          if (body['status'] == 'success') {
            var err = new AlertDialog(
                contentPadding: EdgeInsets.all(0.0),
                title: new Center(
                    child: new Container(
                        color: Colors.orangeAccent,
                        child: new Center(
                            child: new Icon(
                          Icons.mail_outline,
                          color: Colors.white,
                          size: 120.0,
                        )))),
                content: new Container(
                  height: 100.0,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                          child: new Text(
                        body['message'],
                        textAlign: TextAlign.center,
                        softWrap: true,
                      )),
                      new Container(
                        child: new FlatButton(
                          //color: Colors.orangeAccent,
                          child: new Text(
                            'Ok',
                            style: new TextStyle(
                                color: Colors.orangeAccent, fontSize: 20.0),
                          ),
                          onPressed: () {
                            // Navigator.pushReplacementNamed(context,'/home');
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ));
            showDialog(context: context, child: err, barrierDismissible: false);
            GetRent gr = new GetRent();
            gr.getrents();
          } else {}
        },
      );
    } else {
      setState(() {
        isloading = false;
      });
    }
  }

  void _onchangeserv(int se) {
    setState(() {
      _selected = se;
    });
  }

  void _onchangecar(String value) {
    setState(() {
      inicar = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      backgroundColor: Colors.teal,
      appBar: appbar('Rental', context, '/aboutpage'),
      body: Container(
        child: new Container(
          child: new SingleChildScrollView(
              child: new Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.09,
                bottom: 0.0,
                right: 10.0,
                left: 10.0),
            child: new Card(
              elevation: 30.0,
              child: new Form(
                key: formKey,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      child: new Image(
                        image: logo,
                        height: 0.1 * MediaQuery.of(context).size.height,
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(
                        left: leftpadding,
                        right: leftpadding * 3,
                        //bottom: leftpadding
                      ),
                      child: new TextFormField(
                        validator: (val) =>
                            val.isEmpty ? 'Please Input Pickup point' : null,
                        controller: pickup,
                        decoration: new InputDecoration(
                            labelText: 'Pickup point: ',
                            labelStyle: new TextStyle(color: Colors.orange)),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(
                        left: leftpadding,
                        right: leftpadding * 3,
                        //bottom: leftpadding
                      ),
                      child: new TextFormField(
                        validator: (val) =>
                            val.isEmpty ? 'Please Input Destination' : null,
                        controller: destination,
                        decoration: new InputDecoration(
                            labelText: 'Destination: ',
                            labelStyle: new TextStyle(color: Colors.orange)),
                      ),
                    ),
                    new Row(children: <Widget>[
                      new Container(
                        //width: screenwidth * 0.23,
                        margin: EdgeInsets.only(
                            left: leftpadding, top: topdownpadding),
                        child: new Text(
                          'Vehicle :',
                          style: new TextStyle(
                              fontSize: 15.0, color: Colors.orange),
                        ),
                      ),
                      new Center(
                        child: new Container(
                            margin: EdgeInsets.only(
                                left: leftpadding, top: topdownpadding),
                            // width: 400.0,
                            //margin: EdgeInsets.only(),
                            child: new DropdownButton(
                              onChanged: (dynamic value) {
                                _onchangecar(value);
                              },
                              value: inicar,
                              items: cars.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Container(
                                    //width: screenwidth * 0.5,
                                    child: new Text(value),
                                  ),
                                );
                              }).toList(),
                            )),
                      ),
                    ]),
                    new Container(
                      margin: EdgeInsets.only(
                        left: leftpadding,
                        right: leftpadding * 3,
                        //bottom: leftpadding
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: DateTimePickerFormField(
                              dateOnly: true,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2017),
                              lastDate: DateTime(2030),
                              initialTime: TimeOfDay.now(),
                              controller: startdate,
                              enabled: true,
                              validator: (val) => val == null
                                  ? 'Cannot leave Start Date blank'
                                  : null,
                              decoration: new InputDecoration(
                                  labelText: 'Start Date',
                                  labelStyle:
                                      new TextStyle(color: Colors.orange)),
                              format: dateFormat,
                              onChanged: (date) {
                                setState(() {
                                  start = date;
                                });
                              },
                            ),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: DateTimePickerFormField(
                              dateOnly: true,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2005),
                              lastDate: DateTime(2030),
                              initialTime: TimeOfDay.now(),
                              controller: enddate,
                              enabled: true,
                              validator: (val) => val == null
                                  ? 'Cannot leave End Date blank'
                                  : null,
                              decoration: new InputDecoration(
                                  labelText: 'End Date',
                                  labelStyle:
                                      new TextStyle(color: Colors.orange)),
                              format: dateFormat,
                              onChanged: (date) {
                                setState(() {
                                  end = date;
                                });
                                calcDatediff(start, end);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: topdownpadding),
                      child: new Row(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Container(
                                  width: screenwidth * 0.15,
                                  margin: EdgeInsets.only(
                                      left: leftpadding, top: topdownpadding),
                                  child: new Text('Rental')),
                              new Container(
                                margin: EdgeInsets.only(left: leftpadding),
                                child: new Radio(
                                  groupValue: _selected,
                                  value: 0,
                                  onChanged: (int value) {
                                    _onchangeserv(value);
                                  },
                                ),
                              ),
                            ],
                          ),
                          new Row(
                            children: <Widget>[
                              new Container(
                                  width: screenwidth * 0.15,
                                  margin: EdgeInsets.only(
                                      left: leftpadding, top: topdownpadding),
                                  child: new Text('Haulage')),
                              new Container(
                                margin: EdgeInsets.only(left: leftpadding),
                                child: new Radio(
                                  groupValue: _selected,
                                  value: 1,
                                  onChanged: (int value) {
                                    _onchangeserv(value);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    !isloading
                        ? new Container(
                            width: 700.0,
                            height: 50.0,
                            margin: EdgeInsets.only(
                              top: 20.0,
                            ),
                            child: new RaisedButton(
                              elevation: 5.0,
                              color: Colors.orangeAccent,
                              onPressed: () {
                                postForm();
                              },
                              child: new Text(
                                "Create",
                                style: new TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ))
                        : new CircularProgressIndicator(),
                  ],
                ),
              ),
            ),
          )),
        ),
      ),
    );
  }

  String calcDatediff(DateTime st, DateTime end) {
    print("This is calcDiff ${end.difference(st).inDays}");
    return (end.difference(st).inDays).toString();
    //   print(startdate.text);
  }
}
