import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


var head = new AssetImage('assets/danfoman.jpg');

var truck = new AssetImage('assets/delivery-truck.ico');

var car = new AssetImage('assets/sedan.ico');

var calendar = new AssetImage('assets/calendar.ico');

var bus = new AssetImage('assets/bus.png');

//A TextField that has a label(d) and hint(example)
Widget textf(String d, example, TextEditingController t) {
  return new Container(
    padding: EdgeInsets.only(left: 10.0),
    margin: EdgeInsets.only(left: 18.0, right: 18.0, top: 10.0, bottom: 10.0),
    child: new TextField(
      controller: t,
      decoration: new InputDecoration(labelText: d, hintText: example),
    ),
  );
}

//Drop down list where you provide the Heading text(s),and the list(p)

Widget droppd(String i) {
  return new DropdownMenuItem(
    child: new Container(
      margin: EdgeInsets.only(
        left: 25.0,
      ),
      width: 245.0,
      child: new Text(i),
    ),
  );
}

//Creates a card with unknown number of list items
Widget cardmaker(List<String> s) {
  return new Container(
    //margin: EdgeInsets.only(top: 30.0),
    child: new Card(
        elevation: 6.0,
        child: new Container(
          margin: EdgeInsets.only(top: 20.0),
          height: 200.0,
          child: new ListView.builder(
            controller: new ScrollController(keepScrollOffset: false),
            scrollDirection: Axis.vertical,
            itemCount: s.length,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return new Container(
                  child: new Text(
                    s[index],
                    style: new TextStyle(fontSize: 20.0),
                  ),
                );
              }
              return new Container(
                  child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(left: 5.0),
                        height: 5.0,
                        width: 5.0,
                        decoration: new BoxDecoration(
                            color: Colors.black, shape: BoxShape.circle),
                      ),
                      new Container(
                        width: 0.87 * MediaQuery.of(context).size.width,
                        margin: EdgeInsets.all(10.0),
                        child: new Text(
                          s[index],
                          textAlign: TextAlign.start,
                        ),
                      )
                    ],
                  )
                ],
              ));
            },
          ),
        )),
  );
}

//Card with title,content,image,context
Widget rentalcard(String title, String content, ImageProvider ic,
    BuildContext context, String goto, double swidth) {
  double toppadding = 20.0;
  double bottompadding = 10.0;
  return new Container(
      margin: EdgeInsets.only(bottom: bottompadding * 2),
      child: new Card(
        elevation: 3.0,
        child: new Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(top: toppadding, bottom: bottompadding),
                child: new Text(
                  title,
                  textAlign: TextAlign.left,
                  style: new TextStyle(fontSize: 25.0),
                ),
              ),
              new Container(
                child: new Image(
                  height: 80.0,
                  image: ic,
                ),
              ),
              new Container(
                margin: EdgeInsets.only(
                    left: 5.0,
                    right: 5.0,
                    top: toppadding,
                    bottom: bottompadding),
                child: new Text(
                  content,
                  style: new TextStyle(fontSize: 18.0),
                ),
              ),
              new Container(
                height: 50.0,
                width: swidth,
                margin: EdgeInsets.only(
                  top: toppadding,
                ),
                child: new RaisedButton(
                  color: Colors.orange,
                  onPressed: () {
                    Navigator.of(context).pushNamed(goto);
                  },
                  child: new Text(
                    'Start Here',
                    style: new TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ));
}

Widget appbar(String title, BuildContext context, String goto) {
  void onSignedInError() {
    var alert = new AlertDialog(
      title: new Text(
        "How To Get A Refund",
        textAlign: TextAlign.center,
      ),
      content: new Text(
          "Call support line +233 24 358 4253 OR send a mail to info@rydelogistics.com with details of your refund request i.e. Date, Booking/Reference No., Amount.",
          textAlign: TextAlign.center),
    );
    showDialog(context: context, child: alert);
  }

  return new AppBar(
    title: new Text(
      title,
      style: new TextStyle(
        color: Colors.black,
      ),
      textAlign: TextAlign.center,
    ),
    backgroundColor: Colors.white,
    actions: <Widget>[
            new IconButton(
         icon: new Icon(Icons.account_circle),
         onPressed: (){
           Navigator.of(context).pushNamed('/profilepage');
         },
      ),
      new IconButton(
        tooltip: 'Refund',
        color: Colors.black,
        icon: new Icon(Icons.account_balance_wallet),
        onPressed: () {
          onSignedInError();
        },
      ),
      new IconButton(
        tooltip: 'About Us',
        color: Colors.black,
        icon: new Icon(Icons.info_outline),
        onPressed: () {
          Navigator.of(context).pushNamed(goto);
        },
      ),
      new IconButton(
        tooltip: 'Logout',
        color: Colors.black,
        icon: new Icon(Icons.exit_to_app),
        onPressed: () async {
          Future<SharedPreferences> checklogin =
              SharedPreferences.getInstance();

          Future<Null> storelogin() async {
            final SharedPreferences loginpref = await checklogin;
            loginpref.setString('token', '');
          }

          await storelogin();
          Navigator.of(context).pushReplacementNamed('/login');
        },
      ),
    ],
  );
}

String stat = 'Pending';
String source = 'Accra';
String destination = 'Circle';



// Widget to show title and information about title
Widget tripsforminfo(String title, String info) {
  return new Container(
      margin: EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(right: 10.0),
            child: new Text(
              title,
              style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
            ),
          ),
          new Text(info),
        ],
      ));
}
